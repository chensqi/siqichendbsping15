///////////////////////////////////////////////////////////////////////////////
/////////////  The Header File for the Buffer Manager /////////////////////////
///////////////////////////////////////////////////////////////////////////////


#ifndef BUF_H
#define BUF_H

#include "db.h"
#include "page.h"

#define NUMBUF 20   
// Default number of frames, artifically small number for ease of debugging.

#define HTSIZE 7
// Hash Table size
//You should define the necessary classes and data structures for the hash table, 
// and the queues for LSR, MRU, etc.


/*******************ALL BELOW are purely local to buffer Manager********/

// You should create enums for internal errors in the buffer manager.
enum bufErrCodes  { 
	
};

class Replacer;

class BufMgr {

private: // fill in this area

	class Node {
		public :
			int value ;
			Node *next , *pre ;
			Node() ;
			~Node() ;
	};

	class LinkList { // a linklist type of int, can used by either hashTable or BufMgr
			
		Node *head,*tail;
		
		public :
		LinkList() ;
		~LinkList() ;
		
		bool contains( int value ) ;
		int indexOfValue( int value ) ;
		int valueOfIndex( int index ) ;
		bool isEmpty() ;
		int first() ;
		int pollFirst() ;	//	return -1 when is empty
		bool removeFirst() ;	//	return false when is empty

		int last();
		int pollLast() ;
		bool removeLast() ;

		bool removeValue( int value ) ;//	return false when nonexist
		void deleteNode( Node *node ) ; //	make sure node is not head or tail

		void addFirst( int value ) ;
		void addLast( int value ) ;
		
		Node *getLastNode() {
			if ( tail->pre ==  head )
				return NULL ;
			return tail->pre ;
		}
	};
	class HashTable {
		
		LinkList pageTable[HTSIZE] ;
		LinkList frameTable[HTSIZE] ;
		static const int a = 5 , b = 9 ;

		public :
		HashTable() ;
		~HashTable();
	
		int hashFunction( PageId x ) { return (a*x+b) % HTSIZE ; }
		int findFrameNumber( PageId page ) ;	//	return -1 when nonexist
		bool deleteEntry( PageId page ) ;
		bool insertEntry( PageId page, int frameNum ) ;
		
	};
	
	class Descriptor {
		
		public :
		
		PageId page ;
		int pinCount ;
		bool dirty , love ;
		Node *position ;
		
		Descriptor() {
			dirty = false ;
			love = false ;
			pinCount = 0 ;
			page = -1 ;
			position = NULL ;
		}
	};

	HashTable hashTable ;
	LinkList freeList, LoveList, hateList ;
	int sizeOfBuf;

public:

    Page* bufPool; // The actual buffer pool

	Descriptor *bufDescr ;
    BufMgr (int numbuf, Replacer *replacer = 0); 
    // Initializes a buffer manager managing "numbuf" buffers.
	// Disregard the "replacer" parameter for now. In the full 
  	// implementation of minibase, it is a pointer to an object
	// representing one of several buffer pool replacement schemes.

    ~BufMgr();           // Flush all valid dirty pages to disk
	void deleteNode( Node *& ) ;

    Status pinPage(PageId PageId_in_a_DB, Page*& page, int emptyPage=0);
        // Check if this page is in buffer pool, otherwise
        // find a frame for this page, read in and pin it.
        // also write out the old page if it's dirty before reading
        // if emptyPage==TRUE, then actually no read is done to bring
        // the page

    Status unpinPage(PageId globalPageId_in_a_DB, int dirty, int hate);
        // hate should be TRUE if the page is hated and FALSE otherwise
        // if pincount>0, decrement it and if it becomes zero,
        // put it in a group of replacement candidates.
        // if pincount=0 before this call, return error.

    Status newPage(PageId& firstPageId, Page*& firstpage, int howmany=1); 
        // call DB object to allocate a run of new pages and 
        // find a frame in the buffer pool for the first page
        // and pin it. If buffer is full, ask DB to deallocate 
        // all these pages and return error

    Status freePage(PageId globalPageId); 
        // user should call this method if it needs to delete a page
        // this routine will call DB to deallocate the page 

    Status flushPage(PageId pageid);
        // Used to flush a particular page of the buffer pool to disk
        // Should call the write_page method of the DB class

    Status flushAllPages();
	// Flush all pages of the buffer pool to disk, as per flushPage.

    /* DO NOT REMOVE THIS METHOD */    
    Status unpinPage(PageId globalPageId_in_a_DB, int dirty=FALSE)
        //for backward compatibility with the libraries
    {
      return unpinPage(globalPageId_in_a_DB, dirty, FALSE);
    }
};

#endif
