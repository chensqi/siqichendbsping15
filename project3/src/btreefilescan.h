/*
 * btreefilescan.h
 *
 * sample header file
 *
 */
 
#ifndef _BTREEFILESCAN_H
#define _BTREEFILESCAN_H

#include "btfile.h"

// errors from this class should be defined in btfile.h

class BTreeFileScan : public IndexFileScan {
public:
    friend class BTreeFile;

    // get the next record
    Status get_next(RID &, void *);

    // delete the record currently scanned
    Status delete_current();

    int keysize(); // size of the key

    // destructor
    ~BTreeFileScan();
	BTreeFileScan() {
		low = high = NULL ;
		valid = false ;
		keySize = 0 ;
		first = true ;
	}
	void init() {
		low = high = NULL ;
		valid = false ;
		keySize = 0 ;
		first = true ;
	}
	void updateValid() ;
	Status nextRid( );
private:
	const Keytype *low , *high ;
	RID curRid ;
	int keySize ;
	bool valid , first ;
	AttrType keyType;

};

#endif
